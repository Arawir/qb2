########################################################################################
# Please do not change #################################################################
########################################################################################

import os
import time
import numpy as np
import matplotlib.pyplot as plt
from math import comb
    
def isNewData(oldTimestamp):                # you don't need this function
    file = open("output.txt","r")
    data = file.read()
    if data == "":
        return False
    #print(data)
    cells = data.splitlines()[-1].split()
    if cells[0] != "DONE":
        return False
    if int(cells[1]) <= oldTimestamp:
        return False    
    return True

def executeCommand(command):               # you rather don't need this function
    file = open("commands.txt","w")
    file.write(command)
    file.close()
    time.sleep(0.2)
    
def executeCommandAndWaitUntilReady(command):    # you need this function
    file = open("commands.txt","w")
    oldTimestamp = round(time.time() * 1000)
    file.write(command)
    file.close()
    time.sleep(0.1)
    while False==isNewData(oldTimestamp):
        time.sleep(0.1)
    
def getDataOutput():                    # get output of kernel, whatever it is
    file = open("output.txt","r")
    data = file.read().splitlines()[0]
    file.close()
    return data.split()

def getDataOutputF():                    # get output of kernel and convert to float 
    file = open("output.txt","r")
    data = file.read().splitlines()[0]
    file.close()
    out = []
    for x in data.split():
        if "(" in x:
            x = x.replace("(","")
            x = x.replace(")","")
            r = float(x.split(",")[0])
            i = float(x.split(",")[1])
            out.append(r+1j*i)
        else:
            out.append(float(x))
    return out

def getDataOutputFR():                  # get output of kernel and convert to float (only real part)
    file = open("output.txt","r")
    data = file.read().splitlines()[0]
    file.close()
    out = []
    for x in data.split():
        if "(" in x:
            x = x.replace("(","")
            x = x.replace(")","")
            r = float(x.split(",")[0])
            i = float(x.split(",")[1])
            out.append(r)
        else:
            out.append(float(x))
    return out


########################################################################################
########################################################################################
########################################################################################
executeCommandAndWaitUntilReady("clear_workspace ;")


for M in range(4,30):
    for N in range(4,5):
        tStart = int(time.time() * 1000)
        executeCommandAndWaitUntilReady("print_memory ;")
        memStart = int(getDataOutput()[1].split("[")[0])

        executeCommandAndWaitUntilReady("generate_base base "+str(M)+" "+str(N)+" ;")
        executeCommandAndWaitUntilReady("generate_operator_EBHH base H -1 5 -2 0 0 ;")
        executeCommandAndWaitUntilReady("compute_eigenstates H 0 -1 ;")

        executeCommandAndWaitUntilReady("print_memory ;")
        memEnd = int(getDataOutput()[1].split("[")[0])
        executeCommandAndWaitUntilReady("clear_workspace ;")
        tEnd = int(time.time() * 1000)
        dTime = tEnd-tStart
        dMem = memStart-memEnd
        states = comb(N+M-1,N)
        print(f"{M} {N} {states} {dTime} {dMem}")



