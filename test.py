########################################################################################
# Please do not change #################################################################
########################################################################################

import os
import time
import numpy as np
import matplotlib.pyplot as plt
    
def isNewData(oldTimestamp):                # you don't need this function
    file = open("output.txt","r")
    data = file.read()
    if data == "":
        return False
    #print(data)
    cells = data.splitlines()[-1].split()
    if cells[0] != "DONE":
        return False
    if int(cells[1]) <= oldTimestamp:
        return False    
    return True

def executeCommand(command):               # you rather don't need this function
    file = open("commands.txt","w")
    file.write(command)
    file.close()
    time.sleep(0.2)
    
def executeCommandAndWaitUntilReady(command):    # you need this function
    file = open("commands.txt","w")
    oldTimestamp = round(time.time() * 1000)
    file.write(command)
    file.close()
    time.sleep(0.1)
    while False==isNewData(oldTimestamp):
        time.sleep(0.1)
    
def getDataOutput():                    # get output of kernel, whatever it is
    file = open("output.txt","r")
    data = file.read().splitlines()[0]
    file.close()
    return data.split()

def getDataOutputF():                    # get output of kernel and convert to float 
    file = open("output.txt","r")
    data = file.read().splitlines()[0]
    file.close()
    out = []
    for x in data.split():
        if "(" in x:
            x = x.replace("(","")
            x = x.replace(")","")
            r = float(x.split(",")[0])
            i = float(x.split(",")[1])
            out.append(r+1j*i)
        else:
            out.append(float(x))
    return out

def getDataOutputFR():                  # get output of kernel and convert to float (only real part)
    file = open("output.txt","r")
    data = file.read().splitlines()[0]
    file.close()
    out = []
    for x in data.split():
        if "(" in x:
            x = x.replace("(","")
            x = x.replace(")","")
            r = float(x.split(",")[0])
            i = float(x.split(",")[1])
            out.append(r)
        else:
            out.append(float(x))
    return out


########################################################################################
########################################################################################
########################################################################################
executeCommandAndWaitUntilReady("clear_workspace ;")
tStart = int(time.time() * 1000)

test = open("reference.data").read()
lines = test.splitlines()

for line in lines:
    if line == "":
        continue
    if line[0]=="#":
        continue
    cells = line.split()
   # print(cells)
    if "_" in cells[0]:
        cells = line.split("_")
        N=int(cells[0])
        M=int(cells[1])
        PBC=int(cells[2])
        J=-1
        U=float(cells[3])
        Unn=float(cells[4])
        μ=float(cells[5])

        executeCommandAndWaitUntilReady("generate_base base1 "+str(M)+" "+str(N)+" -1 ;")
        executeCommandAndWaitUntilReady("generate_operator_EBHH base1 H "+str(J)+" "+str(U)+" "+str(Unn)+" "+str(μ)+" "+str(PBC)+" ;")
        executeCommandAndWaitUntilReady("generate_operator_Nis base1 Nis ;")
        executeCommandAndWaitUntilReady("generate_operator_G2s base1 G2s 1 ;")
        executeCommandAndWaitUntilReady("compute_eigenstate_name H 0 H_GS ;")
        executeCommandAndWaitUntilReady("compute_expected_value H H_GS ;")
        ED_E = getDataOutputFR()[0]
#        executeCommandAndWaitUntilReady("compute_expected_value Nis H_GS ;")
#        ED_Nis = getDataOutputFR()
#        executeCommandAndWaitUntilReady("compute_expected_value G2s H_GS ;")
#        ED_G2s = getDataOutputFR() 
        executeCommandAndWaitUntilReady("clear_workspace ;")
    if cells[0]=="Sweep=":
        DMRG_E = float(cells[cells.index("E=")+1])
        if abs(DMRG_E - ED_E)<1e-5*abs(ED_E):
            print("OK: N="+str(N)+" M="+str(M)+" PBC="+str(PBC)+" J="+str(J)+" U="+str(U)+" Unn="+str(Unn)+" μ="+str(μ))
        else:
            print("ERROR: (E_DMRG="+str(DMRG_E)+" E_ED="+str(ED_E)+") N="+str(N)+" M="+str(M)+" PBC="+str(PBC)+" J="+str(J)+" U="+str(U)+" Unn="+str(Unn)+" μ="+str(μ))

        
tEnd = int(time.time() * 1000)

print("Time of tests: " + str((tEnd-tStart)/1000) + " [s]")

executeCommandAndWaitUntilReady("print_memory ;")
print( "Mem after tests: "+str(getDataOutput()) )



