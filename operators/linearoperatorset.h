#ifndef LINEAROPERATORSET_H
#define LINEAROPERATORSET_H

#include "linearoperator.h"

class LinearOperatorSet : public iLinearOperator
{
public:
    mBase &base;
    std::list<LinearOperator> operators;
    std::string name;
    ParamVector params;
public:
    LinearOperatorSet(mBase &base_, std::string name_, ParamVector params_);
    ~LinearOperatorSet() = default;

    std::vector<cpx> expVal(StateSet& state_);
    StateSet act(StateSet stateSet_);
    std::vector<cpx> variance(StateSet& state_);
    StateSet* eigenstates(sluint indexMin_, sluint indexMax_, int m=-1, Eigensolver solver=Eigensolver::arma);
    std::string getname(){ return name; }
    EigensBase* generateEigensBase(std::string name_){assert(false);}
    void setParam(std::string paramName_, std::string paramValue_);
};

#endif // LINEAROPERATORSET_H
