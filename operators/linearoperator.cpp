#include "linearoperator.h"
#include <cassert>
#include "../kernel/lanczos.h"

LinearOperator::LinearOperator(mBase &base_, std::string name_, ParamVector params_) :
    base{base_}
  , name{name_}
  , matrix(base_.size(),base_.size())
  , params{params_}
{

}

StateSet LinearOperator::act(StateSet stateSet_)
{
    return StateSet(&base, name+"x"+stateSet_.name, arma::cx_mat(matrix*stateSet_.vectors));
}

std::vector<cpx> LinearOperator::expVal(StateSet &state_)
{
    std::vector<cpx> out;
    for(sluint i=0; i<state_.numberOfStates(); i++){
        out.push_back(arma::as_scalar(state_.vectors.col(i).t()*matrix*state_.vectors.col(i)));
    }
    return out;
}

std::vector<cpx> LinearOperator::variance(StateSet &state_)
{
    std::vector<cpx> out;
    for(sluint i=0; i<state_.numberOfStates(); i++){
        out.push_back( arma::as_scalar(state_.vectors.col(i).t()*matrix*matrix*state_.vectors.col(i)) - pow(arma::as_scalar(state_.vectors.col(i).t()*matrix*state_.vectors.col(i)), 2));
    }
    return out;
}

StateSet* LinearOperator::eigenstates(sluint indexMin_, sluint indexMax_, int m, Eigensolver solver)
{
    if(m==-1){ m=base.size()-1;} //iterations
    if(indexMax_==-1) indexMax_ = base.size()-2;
    arma::vec eigval;
    arma::mat eigvec;
    //TODO FIX imaginary!!!!!!!!!!!!!!!!!
    if(solver==Eigensolver::arma){
        arma::eigs_opts opts;
        opts.tol = 0.0;
        opts.maxiter=1000;
 //       opts.subdim = std::min(base.size()-1, std::max(luint(20), 2*indexMax_+1));
        arma::eigs_sym(eigval, eigvec, matrix, indexMax_+1, "sa", opts);
    } else if(solver==Eigensolver::lanczos){
        lanczos(sluint(m),eigval,eigvec,matrix);
    }
//    if(indexMax_ == m){
//        return new StateSet{&base, name+"_eigenstates", eigvec};
//    }
//    return new StateSet{&base, name+"_eigenstates", eigvec.submat(0,indexMin_,base.size()-1,indexMax_)};
    return new StateSet{&base, name+"_eigenstates", eigvec};
}

void LinearOperator::setParam(std::string paramName_, std::string paramValue_)
{
    params.set(paramName_, paramValue_);
}

void vec_push(arma::vec & v, double value) {
    arma::vec av(1);
    av.at(0) = value;
    v.insert_rows(v.n_rows, av.row(0));
}

EigensBase* LinearOperator::generateEigensBase(std::string name_)
{
    arma::vec eigval;
    arma::mat eigvec;
    arma::eigs_sym(eigval, eigvec, matrix, base.size()-1, "sa"); //TODO

    arma::vec eigval2;
    arma::mat eigvec2;
    arma::eigs_sym(eigval2, eigvec2, matrix, 1, "la"); //TODO

    eigvec.insert_cols(eigval.n_rows,eigvec2);
    vec_push(eigval,arma::as_scalar(eigval2[0]));

    EigensBase *out = new EigensBase{name_};
    out->transferMatrix.isUnit = false;
    out->transferMatrix.BToFockMatrix = arma::inv(eigvec.t());
    out->transferMatrix.FockToBMatrix = eigvec.t();
   // out->FockToBMatrix = arma::cx_mat( eigvec.t(), arma::mat(eigvec.n_cols, eigvec.n_rows, arma::fill::zeros));
   // out->BToFockMatrix = arma::cx_mat( arma::inv(eigvec.t()), arma::mat(eigvec.n_cols, eigvec.n_rows, arma::fill::zeros));
    out->eigenValues = eigval;
    out->basicFockBase = base.basicFockBase;

    return out;
}

//////////////////////////////////////////////////////////////////////////


cxLinearOperator::cxLinearOperator(mBase &base_, std::string name_, ParamVector params_) :
    base{base_}
  , name{name_}
  , matrix(base_.size(),base_.size())
  , params{params_}
{

}

StateSet cxLinearOperator::act(StateSet stateSet_)
{
    return StateSet(&base, name+"x"+stateSet_.name, arma::cx_mat(matrix*stateSet_.vectors));
}

std::vector<cpx> cxLinearOperator::expVal(StateSet &state_)
{
    std::vector<cpx> out;
    for(sluint i=0; i<state_.numberOfStates(); i++){
        out.push_back(arma::as_scalar(state_.vectors.col(i).t()*matrix*state_.vectors.col(i)));
    }
    return out;
}

std::vector<cpx> cxLinearOperator::variance(StateSet &state_)
{
    std::vector<cpx> out;
    for(sluint i=0; i<state_.numberOfStates(); i++){
        out.push_back( arma::as_scalar(state_.vectors.col(i).t()*matrix*matrix*state_.vectors.col(i)) - pow(arma::as_scalar(state_.vectors.col(i).t()*matrix*state_.vectors.col(i)), 2));
    }
    return out;
}

StateSet* cxLinearOperator::eigenstates(sluint indexMin_, sluint indexMax_, int m, Eigensolver solver)
{
    if(m==-1){ m=base.size()-1;} //iterations
    if(indexMax_==-1) indexMax_ = base.size()-2;
    arma::vec eigval;
    arma::cx_mat eigvec;
    //TODO FIX imaginary!!!!!!!!!!!!!!!!!
    if(solver==Eigensolver::arma){
        arma::eigs_opts opts;
        opts.tol = 0.0;
        opts.maxiter=1000;
 //       opts.subdim = std::min(base.size()-1, std::max(luint(20), 2*indexMax_+1));
        arma::eig_sym(eigval, eigvec, arma::cx_mat(matrix), "sa");
    } else if(solver==Eigensolver::lanczos){
      //  lanczos(luint(m),eigval,eigvec,matrix);
        assert(false);
    }
//    if(indexMax_ == m){
//        return new StateSet{&base, name+"_eigenstates", eigvec};
//    }
//    return new StateSet{&base, name+"_eigenstates", eigvec.submat(0,indexMin_,base.size()-1,indexMax_)};
    return new StateSet{&base, name+"_eigenstates", eigvec};
}

void cxLinearOperator::setParam(std::string paramName_, std::string paramValue_)
{
    params.set(paramName_, paramValue_);
}


EigensBase* cxLinearOperator::generateEigensBase(std::string name_)
{
//    arma::vec eigval;
//    arma::cx_mat eigvec;
//    arma::eig_sym(eigval, eigvec, arma::cx_mat(matrix)); //TODO

    assert(false);

   // eigvec.insert_cols(eigval.n_rows,eigvec2);
   // vec_push(eigval,arma::as_scalar(eigval2[0]));

    EigensBase *out = new EigensBase{name_};
//    out->transferMatrix.isUnit = false;
//    out->transferMatrix.BToFockMatrix = arma::inv(eigvec.t());
//    out->transferMatrix.FockToBMatrix = eigvec.t();
//   // out->FockToBMatrix = arma::cx_mat( eigvec.t(), arma::mat(eigvec.n_cols, eigvec.n_rows, arma::fill::zeros));
//   // out->BToFockMatrix = arma::cx_mat( arma::inv(eigvec.t()), arma::mat(eigvec.n_cols, eigvec.n_rows, arma::fill::zeros));
//    out->eigenValues = eigval;
//    out->basicFockBase = base.basicFockBase;

    return out;
}


