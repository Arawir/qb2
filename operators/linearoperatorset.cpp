#include "linearoperatorset.h"

LinearOperatorSet::LinearOperatorSet(mBase &base_, std::string name_, ParamVector params_) :
    base{base_}
  , name{name_}
  , params{params_}
{

}

std::vector<cpx> LinearOperatorSet::expVal(StateSet &state_)
{
    std::vector<cpx> out;

    for(sluint i=0; i<state_.numberOfStates(); i++){
        for(auto& oper : operators){
            out.push_back(arma::as_scalar(state_.vectors.col(i).t()*oper.matrix*state_.vectors.col(i)));
        }
    }
    return out;
}

StateSet LinearOperatorSet::act(StateSet stateSet_)
{
    assert(false && "LineatOperatorSet::act is removed!");
}

std::vector<cpx> LinearOperatorSet::variance(StateSet &state_)
{
    assert(false && "LineatOperatorSet::variance is removed!");
}

StateSet* LinearOperatorSet::eigenstates(sluint indexMin_, sluint indexMax_, int m, Eigensolver solver)
{
    assert(false && "LineatOperatorSet::eigenstates is removed!");
}

void LinearOperatorSet::setParam(std::string paramName_, std::string paramValue_)
{
    params.set(paramName_, paramValue_);
}
