#ifndef LINEAROPERATOR_H
#define LINEAROPERATOR_H

#include "ilinearoperator.h"

class LinearOperator : public iLinearOperator
{
public:
    mBase& base;
    arma::sp_mat matrix;
    std::string name;
    ParamVector params;
public:
    LinearOperator(mBase &base_, std::string name_, ParamVector params_);
    ~LinearOperator() = default;

    StateSet act(StateSet stateSet_);
    EigensBase* generateEigensBase(std::string name_);
    std::vector<cpx> expVal(StateSet& state_);
    std::vector<cpx> variance(StateSet& state_);

    StateSet* eigenstates(sluint indexMin_, sluint indexMax_, int m=-1, Eigensolver solver=Eigensolver::arma);
    std::string getname() override { return name; }

    void setParam(std::string paramName_, std::string paramValue_);
};

class cxLinearOperator : public iLinearOperator
{
public:
    mBase& base;
    arma::sp_cx_mat matrix;
    std::string name;
    ParamVector params;
public:
    cxLinearOperator(mBase &base_, std::string name_, ParamVector params_);
    ~cxLinearOperator() = default;

    StateSet act(StateSet stateSet_);
    EigensBase* generateEigensBase(std::string name_);
    std::vector<cpx> expVal(StateSet& state_);
    std::vector<cpx> variance(StateSet& state_);

    StateSet* eigenstates(sluint indexMin_, sluint indexMax_, int m=-1, Eigensolver solver=Eigensolver::arma);
    std::string getname() override { return name; }

    void setParam(std::string paramName_, std::string paramValue_);
};





#endif // LINEAROPERATOR_H
