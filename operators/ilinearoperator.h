#ifndef ILINEAROPERATOR_H
#define ILINEAROPERATOR_H


#include "../states/state.h"
#include "../kernel/param.h"
#include <list>


enum class Eigensolver{ arma, lanczos };

class iLinearOperator{
public:
    iLinearOperator(){}
    ~iLinearOperator() = default;
    virtual StateSet act(StateSet stateSet_) = 0;
    virtual std::vector<cpx> expVal(StateSet& state_) = 0;
    virtual std::vector<cpx> variance(StateSet& state_) = 0;
    virtual StateSet* eigenstates(sluint indexMin_, sluint indexMax_, int m=-1, Eigensolver solver=Eigensolver::arma) = 0;
    virtual std::string getname() = 0;
    virtual EigensBase* generateEigensBase(std::string name_) = 0;
    virtual void setParam(std::string paramName_, std::string paramValue_) = 0;
};
#endif // ILINEAROPERATOR_H
