#include "state.h"
#include <cassert>

#include <iomanip>
#include <complex>
#include <cmath>

using namespace std::complex_literals;

StateSet::StateSet(mBase* base_, std::string name_, arma::cx_mat vectors_):
    vectors{vectors_}
  , base{base_}
  , name{name_}
{

}

StateSet::StateSet(mBase* base_, std::string name_, arma::mat vectors_):
  base{base_}
  , name{name_}
{
    vectors = arma::cx_mat(vectors_, arma::mat(vectors_.n_rows, vectors_.n_cols, arma::fill::zeros));
}

StateSet::StateSet(mBase *base_, std::string name_):
    base{base_}
    , name{name_}
{

}

StateSet::StateSet(const StateSet &obj, std::string name_) :
    vectors{obj.vectors}
  , base{obj.base}
  , name{name_}
{

}

luint StateSet::numberOfStates()
{
    return vectors.n_cols;
}

StateSet* StateSet::subset(std::string name_, sluint indexMin_, sluint indexMax_)
{
    return new StateSet(base, name_, vectors.submat(0,indexMin_,base->size()-1,indexMax_));
}

void StateSet::setBase(mBase *base_)
{
    assert(base_->basicFockBase==base->basicFockBase);
    if( base->transferMatrix.isUnit ){
        if( !base_->transferMatrix.isUnit ){
            vectors = base_->transferMatrix.FockToBMatrix * vectors;
        }
    } else if( base_->transferMatrix.isUnit ){
        if( !base->transferMatrix.isUnit ){
            vectors = base->transferMatrix.BToFockMatrix * vectors;
        }
    } else {
        vectors = base_->transferMatrix.FockToBMatrix * base->transferMatrix.BToFockMatrix * vectors;
    }
    base = base_;
}

void StateSet::timeEvolve(double tau_)
{
    for(sluint j=0; j<vectors.n_rows; j++){
        vectors.row(j) *= exp(-1i*tau_*base->eigenValues[j]);
      //  std::cout << base->eigenValues[j] << std::endl;
    }
}

arma::cx_mat StateSet::operator*(const StateSet &stateSetR)
{
    auto tmp = vectors.t()*stateSetR.vectors;
    return tmp;
}

void StateSet::save(std::string saveName_)
{
    //vactors.save(saveName_);
}

void StateSet::load(std::string loadName_)
{

}
