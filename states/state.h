#ifndef STATE_H
#define STATE_H

#include "../bases/base.h"
#include <armadillo>


class StateSet{
public:
    arma::cx_mat vectors;
    mBase* base;
    std::string name;
public:
    StateSet(mBase* base_, std::string name_, arma::cx_mat vectors_);
    StateSet(mBase* base_, std::string name_, arma::mat vectors_);
    StateSet(mBase* base_, std::string name_);
    StateSet(const StateSet& obj, std::string name_);
    ~StateSet() = default;
    luint numberOfStates();
    StateSet* subset(std::string name_, sluint indexMin_, sluint indexMax_);
    void setBase(mBase* base_);
    void timeEvolve(double tau_); //only in base of eigens

    arma::cx_mat operator*(const StateSet& stateSetR);
    void save(std::string saveName_);
    void load(std::string loadName_);
};

#endif // STATE_H
