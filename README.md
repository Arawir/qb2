## Installation

    $ cmake .
    $ make

Warning: armadillo library is required!
    

## Run 

The "whole" application is divided into kernel (C++) and ueser interface (Python). You have to run kernel (./qb2) and then execute python script (python example.py):

    $ ./qb2
    $ python example.py

## Commands

- executeCommandAndWaitUntilReady("generate_base    name M N ;")
- executeCommandAndWaitUntilReady("generate_base_eigens     name operatorName ;")
- executeCommandAndWaitUntilReady("set_base     newBaseName stateName ;")
- executeCommandAndWaitUntilReady("timeEvolve   stateToEvolveName tau ;")
- executeCommandAndWaitUntilReady("generate_operator_EBHH   baseName operatorName J U Unn μ PBC(0/1) ;")
- executeCommandAndWaitUntilReady("generate_operator_Nis    baseName operatorName ;")
- executeCommandAndWaitUntilReady("generate_operator_G1s    baseName operatorName j' ;")
- executeCommandAndWaitUntilReady("generate_operator_G2s    baseName operatorName j' ;")
- executeCommandAndWaitUntilReady("compute_eigenstate     operatorName eigenstateNumber ;") #0-th eigenstate is a ground-state
- executeCommandAndWaitUntilReady("compute_eigenstate_name      operatorName eigenstateNumber outputStateName ;")
- executeCommandAndWaitUntilReady("compute_eigenstates      operatorName minEigenstateNumber maxEigenstateNumber ;") #maxEigenstateNumber=-1 is the last state
- executeCommandAndWaitUntilReady("compute_expected_value   operatorName stateName ;")
- executeCommandAndWaitUntilReady("compute_variance     operatorName stateName ;")
- executeCommandAndWaitUntilReady("compute_overlap      braStateNames ketStateNames ;")  #<bra | ket>
- executeCommandAndWaitUntilReady("print_bases ;")
- executeCommandAndWaitUntilReady("print_states ;")
- executeCommandAndWaitUntilReady("print_operators ;")
- executeCommandAndWaitUntilReady("clear_workspace ;")
- executeCommandAndWaitUntilReady("quit ;")



- outputRaw = getDataOutput() <- get raw output from kernel
- outputComplexFloat = getDataOutputF() <- get output from kernel and convert it into complex 
- outputRealFloat = getDataOutputFR() <- get output from kernel and convert it into float 

## Examples

- example.py <- compute GS of B-H Hamiltonian and plot correlations
- example2.py <- compute overlaps between GS of repulsive Hamiltonian and eigenstates of attractive Hamiltonian
- example3.py <- simple time evolution after quench 

## Hamiltonian 

In my convention extended bose-hubbard hamiltonian is defined as follow:

![plot](./EBH.png)

