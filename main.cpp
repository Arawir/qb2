﻿#include "./kernel/storage.h"
#include "./bases/fockbase.h"

int main(int argc, char **argv){
    Storage storage;

    while(1){
        try{
            auto [command,commandParams] = getCommand();
            if(command=="quit"){
                clearOutput(); checkNumberOfParams(commandParams, 0);
                break;

            } else if(command=="generate_base"){
                clearOutput();
                if(commandParams.size()==3){
                    commandParams.push_back("-1"); //add maxOcc
                }
                ParamVector params{commandParams, {"name","M","N","maxOcc"} };
                std::string name = params.popString("name");

                storage.bases.push_back(new FockBase{name,params});
              //  storage.findBase(name)->printStates();
                writeToOutput("Base generated correctly");

            } else if(command=="generate_base_eigens"){
                clearOutput(); checkNumberOfParams(commandParams, 2);

                std::string name = commandParams[0];
                iLinearOperator* oper = storage.findOperator(commandParams[1]);

                storage.bases.push_back(oper->generateEigensBase(name));
                writeToOutput("Base generated correctly");

            } else if(command=="set_base"){
                clearOutput(); checkNumberOfParams(commandParams, 2);

                mBase* base = storage.findBase(commandParams[0]);
                StateSet* state = storage.findState(commandParams[1]);

                state->setBase(base);
                writeToOutput("Base changed correctly");

            } else if(command=="timeEvolve"){
                clearOutput(); checkNumberOfParams(commandParams, 2);

                StateSet* state = storage.findState(commandParams[0]);
                double tau = atof(commandParams[1].c_str());

                state->timeEvolve(tau);
                writeToOutput("State evovled correctly");

            } else if(command=="generate_operator_BHH"){
                clearOutput();
                ParamVector params{commandParams, {"baseName","operName","J","U","mu","PBC"} };

                mBase* base = storage.findBase( params.popString("baseName") );
                std::string name = params.popString("operName");

                storage.linearOperators.push_back(new BHHamiltonian(*base, name, params));
                writeToOutput("BHHamiltonian generated correctly");

            } else if(command=="generate_operator_EBHH"){
                clearOutput();
                ParamVector params{commandParams, {"baseName","operName","J","U","Unn","mu","PBC"} };

                mBase* base = storage.findBase( params.popString("baseName") );
                std::string name = params.popString("operName");

                storage.linearOperators.push_back(new EBHHamiltonian(*base, name, params));
                writeToOutput("EBHHamiltonian generated correctly");

            } else if(command=="generate_operator_RBHH"){
                clearOutput();
                ParamVector params{commandParams, {"baseName","operName","J","U","Omega","mu","PBC"} };

                mBase* base = storage.findBase( params.popString("baseName") );
                std::string name = params.popString("operName");

                storage.linearOperators.push_back(new RBHHamiltonian(*base, name, params));
                writeToOutput("RBHHamiltonian generated correctly");

            } else if(command=="generate_operator_Momentum"){
                clearOutput();
                ParamVector params{commandParams, {"baseName","operName","PBC"} };

                mBase* base = storage.findBase( params.popString("baseName") );
                std::string name = params.popString("operName");

                storage.linearOperators.push_back(new Momentum(*base, name, params));
                writeToOutput("Momentum operator generated correctly");

            } else if(command=="generate_operator_Nis"){
                clearOutput();
                ParamVector params{commandParams, {"baseName","operName"} };

                mBase* base = storage.findBase( params.popString("baseName") );
                std::string name = params.popString("operName");

                storage.linearOperators.push_back(new Nis(*base, name, params));
                writeToOutput("Nis generated correctly");

            } else if(command=="generate_operator_G1s"){
                clearOutput();
                ParamVector params{commandParams, {"baseName","operName","m"} };

                mBase* base = storage.findBase( params.popString("baseName") );
                std::string name = params.popString("operName");

                storage.linearOperators.push_back(new G1s(*base, name, params));
                writeToOutput("G1s generated correctly");

            } else if(command=="generate_operator_G2s"){
                clearOutput();
                ParamVector params{commandParams, {"baseName","operName","m"} };

                mBase* base = storage.findBase( params.popString("baseName") );
                std::string name = params.popString("operName");

                storage.linearOperators.push_back(new G2s(*base, name, params));
                writeToOutput("G2s generated correctly");

            } else if(command=="set_param"){
                clearOutput();
                ParamVector params{commandParams, {"operName", "paramName", "paramNewValue"} };

                iLinearOperator* oper = storage.findOperator(params.popString("operName"));
                //oper->setParam(params);
                writeToOutput("Parameter set correctly");

            } else if(command=="compute_eigenstate"){
                clearOutput(); checkNumberOfParams(commandParams, 2,4);

                iLinearOperator* oper = storage.findOperator(commandParams[0]);
                sluint stateNum = sluint(std::stoi(commandParams[1]));
                if(commandParams.size()==2){
                    storage.states.push_back(oper->eigenstates(stateNum,stateNum));
                } else if(commandParams.size()==3){
                    Eigensolver solver = Eigensolver::arma;
                    if(commandParams[2]=="lanczos"){
                        solver = Eigensolver::lanczos;
                    }
                    storage.states.push_back(oper->eigenstates(stateNum,stateNum,-1, solver));
                } else if(commandParams.size()==4){
                    Eigensolver solver = Eigensolver::arma;
                    if(commandParams[2]=="lanczos"){
                        solver = Eigensolver::lanczos;
                    }
                    sluint m = sluint(std::stoi(commandParams[3]));
                    storage.states.push_back(oper->eigenstates(stateNum,stateNum, m, solver));
                }
                writeToOutput("Eigenstate computed correctly");

            } else if(command=="compute_eigenstate_name"){
                clearOutput(); checkNumberOfParams(commandParams, 3);

                iLinearOperator* oper = storage.findOperator(commandParams[0]);
                sluint stateNum = sluint(std::stoi(commandParams[1]));
                std::string stateName = commandParams[2];
                auto s = oper->eigenstates(stateNum,stateNum);
                s->name = stateName;
                storage.states.push_back(s);
                writeToOutput("Eigenstate computed correctly");

            } else if(command=="compute_eigenstates"){
                clearOutput(); checkNumberOfParams(commandParams, 3);

                iLinearOperator* oper = storage.findOperator(commandParams[0]);
                sluint stateNumMin = sluint(std::stoi(commandParams[1]));
                sluint stateNumMax = sluint(std::stoi(commandParams[2]));

                storage.states.push_back(oper->eigenstates(stateNumMin,stateNumMax));
                writeToOutput("Eigenstates computed correctly");

            } else if(command=="get_substateset"){
                clearOutput(); checkNumberOfParams(commandParams, 4);

                auto state = storage.findState(commandParams[0]);
                std::string newStateName = commandParams[1];
                sluint stateNumMin = sluint(std::stoi(commandParams[2]));
                sluint stateNumMax = sluint(std::stoi(commandParams[3]));

                storage.states.push_back(state->subset(newStateName,stateNumMin, stateNumMax));
                writeToOutput("Extracting subset done correctyly!");

            } else if(command=="compute_expected_value"){
                clearOutput(); checkNumberOfParams(commandParams, 2);

                iLinearOperator* oper = storage.findOperator(commandParams[0]);
                StateSet* state = storage.findState(commandParams[1]);

                writeToOutput(oper->expVal(*state));

            } else if(command=="compute_variance"){
                clearOutput(); checkNumberOfParams(commandParams, 2);

                iLinearOperator* oper = storage.findOperator(commandParams[0]);
                StateSet* state = storage.findState(commandParams[1]);

                writeToOutput(oper->variance(*state));

            } else if(command=="compute_overlap"){
                clearOutput(); checkNumberOfParams(commandParams, 2);

                StateSet* statL = storage.findState(commandParams[0]);
                StateSet* statR = storage.findState(commandParams[1]);

                writeToOutputM((*statL)*(*statR));

            } else if(command=="copy"){
                clearOutput(); checkNumberOfParams(commandParams, 2);

                if(storage.stateExists(commandParams[0])){
                    StateSet* state = storage.findState(commandParams[0]);
                    storage.states.push_back(new StateSet{*state,commandParams[1]});
                }
                writeToOutput("Copy done correctly");

            } else if(command=="print_bases"){
                clearOutput(); checkNumberOfParams(commandParams, 0);

                std::string out;
                for(auto& base : storage.bases){
                    out.append(base->name);
                    out.append(" ");
                }
                writeToOutput(out);

            } else if(command=="print_states"){
                clearOutput(); checkNumberOfParams(commandParams, 0);

                std::string out;
                for(StateSet* state : storage.states){
                    out.append(state->name);
                    out.append(" ");
                }
                writeToOutput(out);

            } else if(command=="print_operators"){
                clearOutput(); checkNumberOfParams(commandParams, 0);

                std::string out;
                for(iLinearOperator* oper : storage.linearOperators){
                    out.append(oper->getname());
                    out.append(" ");
                }
                writeToOutput(out);

            } else if(command=="print_memory"){
                clearOutput(); checkNumberOfParams(commandParams, 0);
                std::string memUsedUnit = "[kB]";
                std::string memUsedPeakUnit = "[kB]";
                auto memUsed = process_mem_usage();
                auto memUsedPeak = process_peak_mem_usage();
                if(memUsed >=1000){ memUsed/=1000; memUsedUnit="[MB]"; }
                if(memUsed >=1000){ memUsed/=1000; memUsedUnit="[GB]"; }
                if(memUsedPeak >=1000){ memUsedPeak/=1000; memUsedPeakUnit="[MB]"; }
                if(memUsedPeak >=1000){ memUsedPeak/=1000; memUsedPeakUnit="[GB]"; }

                std::string out ="Mem: " + std::to_string(memUsed) + memUsedUnit;
                out += ", Mem max: " + std::to_string(memUsedPeak) + memUsedPeakUnit;

                writeToOutput(out);

            } else if(command=="clear_workspace"){
                clearOutput(); checkNumberOfParams(commandParams, 0);

                storage.clearWorkspace();
                writeToOutput("Workspace cleared!");

            } else {
                clearOutput();
                writeToOutput("Unknown command!","ERROR");
            }
        } catch(MyException& e) {
            writeToOutput(e.message, "ERROR");
//std::cout << "MyException caught" << std::endl;
           // std::cout << e.what() << std::endl;
        } catch(std::exception& e) {
            writeToOutput("Unknown excetpion", "ERROR");
            //Other errors
        }
    }

    storage.clearWorkspace();
    writeToOutput("Program closed correctly");
}
