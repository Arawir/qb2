#include "rbhhamiltonian.h"
#include "../bases/fockbase.h"

using namespace std::complex_literals;
RBHHamiltonian::RBHHamiltonian(mBase &base_, std::string name_, ParamVector params_):
    cxLinearOperator{base_, name_, params_}
{
    generate();
}

void RBHHamiltonian::setParam(std::string paramName_, std::string paramValue_)
{
}

void RBHHamiltonian::generate()
{
    double J = params.getDouble("J");
    double Omega = params.getDouble("Omega");
    double U = params.getDouble("U");
    double mu = params.getDouble("mu");
    bool PBC = params.getDouble("PBC");

    for(int m=0; m<base.basicFockBase->M()-1; m++){
        base.add_b_bdag(J-1i*Omega,m,m+1,this->matrix);
        base.add_bdag_b(J+1i*Omega,m,m+1,this->matrix);
    }

    if(PBC){
        base.add_b_bdag(J-1i*Omega,base.basicFockBase->M()-1,0,this->matrix);
        base.add_bdag_b(J+1i*Omega,base.basicFockBase->M()-1,0,this->matrix);
    }

    for(int m=0; m<base.basicFockBase->M(); m++){
        base.add_nnm1(U,m,this->matrix);
        base.add_n(mu,m,this->matrix);
    }
}

