#include "ebhhamiltonian.h"
#include "../bases/fockbase.h"

EBHHamiltonian::EBHHamiltonian(mBase &base_, std::string name_, ParamVector params_):
    LinearOperator{base_, name_, params_}
{
    generate();
}

void EBHHamiltonian::setParam(std::string paramName_, std::string paramValue_)
{
    if(paramName_=="J"){
        double dJ = atof(paramValue_.c_str())-params.getDouble("J");
        for(int m=0; m<base.basicFockBase->M()-1; m++){
            base.add_b_bdag(dJ,m,m+1,this->matrix);
            base.add_bdag_b(dJ,m,m+1,this->matrix);
        }

        if(params.getBool("PBC")){
            base.add_b_bdag(dJ,base.basicFockBase->M()-1,0,this->matrix);
            base.add_bdag_b(dJ,base.basicFockBase->M()-1,0,this->matrix);
        }
    } else if(paramName_=="U"){
        double dU = atof(paramValue_.c_str())-params.getDouble("U");
        for(int m=0; m<base.basicFockBase->M(); m++){
            base.add_nnm1(dU,m,this->matrix);
        }
    } else if(paramName_=="Unn"){
        double dUnn = atof(paramValue_.c_str())-params.getDouble("Unn");
        for(int m=0; m<base.basicFockBase->M()-1; m++){
            base.add_nn(dUnn,m,m+1,this->matrix);
        }
        if(params.getBool("PBC")){
            base.add_nn(dUnn,base.basicFockBase->M()-1,0,this->matrix);
        }
    } else if(paramName_=="mu"){
        double dmu = atof(paramValue_.c_str())-params.getDouble("mu");
        for(int m=0; m<base.basicFockBase->M(); m++){
            base.add_n(dmu,m,this->matrix);
        }
    } else if(paramName_=="PBC"){
        if( paramValue_=="1" && params.getBool("PBC")==false){
            base.add_b_bdag(params.getDouble("J"),base.basicFockBase->M()-1,0,this->matrix);
            base.add_bdag_b(params.getDouble("J"),base.basicFockBase->M()-1,0,this->matrix);
            base.add_nn(params.getDouble("Unn"),base.basicFockBase->M()-1,0,this->matrix);
        } else if( paramValue_=="0" && params.getBool("PBC")==true){
            base.add_b_bdag(-params.getDouble("J"),base.basicFockBase->M()-1,0,this->matrix);
            base.add_bdag_b(-params.getDouble("J"),base.basicFockBase->M()-1,0,this->matrix);
            base.add_nn(-params.getDouble("Unn"),base.basicFockBase->M()-1,0,this->matrix);
        }
    } else {
        assert(false && "Unknown param");
    }

    LinearOperator::setParam(paramName_,paramValue_);
}

void EBHHamiltonian::generate()
{
    double J = params.getDouble("J");
    double U = params.getDouble("U");
    double Unn = params.getDouble("Unn");
    double mu = params.getDouble("mu");
    bool PBC = params.getDouble("PBC");

    for(int m=0; m<base.basicFockBase->M()-1; m++){
        base.add_b_bdag(J,m,m+1,this->matrix);
        base.add_bdag_b(J,m,m+1,this->matrix);

        base.add_nn(Unn,m,m+1,this->matrix);
    }

    if(PBC){
        base.add_b_bdag(J,base.basicFockBase->M()-1,0,this->matrix);
        base.add_bdag_b(J,base.basicFockBase->M()-1,0,this->matrix);

        base.add_nn(Unn,base.basicFockBase->M()-1,0,this->matrix);
    }

    for(int m=0; m<base.basicFockBase->M(); m++){
        base.add_nnm1(U,m,this->matrix);
        base.add_n(mu,m,this->matrix);
    }
}
