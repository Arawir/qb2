#ifndef BHHAMILTONIAN_H
#define BHHAMILTONIAN_H

#include "../operators/linearoperator.h"

class BHHamiltonian : public LinearOperator
{
public:
    BHHamiltonian(mBase& base_, std::string name_, ParamVector params_);
    ~BHHamiltonian() = default;

    void setParam(std::string paramName_, std::string paramValue_);
private:
    void generate();
};



#endif // BHHAMILTONIAN_H
