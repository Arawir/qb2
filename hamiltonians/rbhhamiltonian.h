#ifndef RBHHAMILTONIAN_H
#define RBHHAMILTONIAN_H


#include "../operators/linearoperator.h"

class RBHHamiltonian : public cxLinearOperator
{
public:
    RBHHamiltonian(mBase& base_, std::string name_, ParamVector params_);
    ~RBHHamiltonian() = default;

    void setParam(std::string paramName_, std::string paramValue_);
private:
    void generate();
};

#endif // RBHHAMILTONIAN_H
