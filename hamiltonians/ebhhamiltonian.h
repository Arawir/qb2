#ifndef EBHHAMILTONIAN_H
#define EBHHAMILTONIAN_H

#include "../operators/linearoperator.h"

class EBHHamiltonian : public LinearOperator
{
public:
    EBHHamiltonian(mBase& base_, std::string name_, ParamVector params_);
    ~EBHHamiltonian() = default;

    void setParam(std::string paramName_, std::string paramValue_);
private:
    void generate();
};

#endif // EBHHAMILTONIAN_H
