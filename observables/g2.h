#ifndef G2_H
#define G2_H

#include "../operators/linearoperatorset.h"

class G2 : public LinearOperator
{
public:
    G2(mBase& base_, std::string name_, ParamVector params_);
    ~G2() = default;
private:
    void generate();
};

class G2s: public LinearOperatorSet
{
public:
    G2s(mBase& base_, std::string name_, ParamVector params_);
    ~G2s() = default;
private:
    void generate();
};

#endif // G2_H
