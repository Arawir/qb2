#include "g1.h"
#include "../bases/fockbase.h"


G1::G1(mBase &base_, std::string name_, ParamVector params_):
    LinearOperator{base_, name_,params_}
{
    generate();
}

void G1::generate()
{
    int m = params.getInt("m");
    int mp = params.getInt("mp");
    base.add_bdag_b(1.0,m,mp,this->matrix);
//    matrix = base.bdag_b(m,mp);
}



G1s::G1s(mBase &base_, std::string name_, ParamVector params_):
    LinearOperatorSet{base_, name_, params_}
{
    generate();
}

void G1s::generate()
{
    int m = params.getInt("m");
    for(uint mp=0; mp<base.basicFockBase->M(); mp++){
        operators.push_back(G1{base,name+"_"+std::to_string(m), ParamVector{{std::to_string(m),std::to_string(mp)}, {"m","mp"}} });
    }
}
