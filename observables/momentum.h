#ifndef MOMENTUM_H
#define MOMENTUM_H

#include "../operators/linearoperatorset.h"

class Momentum : public cxLinearOperator
{
public:
    Momentum(mBase& base_, std::string name_, ParamVector params_);
    ~Momentum() = default;
private:
    void generate();
};


#endif // MOMENTUM_H
