#include "ni.h"
#include "../bases/fockbase.h"

Ni::Ni(mBase &base_, std::string name_, ParamVector params_):
    LinearOperator{base_, name_, params_}
{
    generate();
}

void Ni::generate()
{
    int m = params.getInt("m");
    base.add_n(1.0,m,this->matrix);
}

Nis::Nis(mBase &base_, std::string name_, ParamVector params_):
    LinearOperatorSet{base_, name_, params_}
{
    generate();
}

void Nis::generate()
{
    for(uint m=0; m<base.basicFockBase->M(); m++){
        operators.push_back(Ni{base,name+"_"+std::to_string(m), ParamVector{{std::to_string(m)}, {"m"}} });
    }
}
