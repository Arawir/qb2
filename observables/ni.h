#ifndef NI_H
#define NI_H

#include "../operators/linearoperatorset.h"

class Ni: public LinearOperator
{
public:
    Ni(mBase& base_, std::string name_, ParamVector params_);
    ~Ni() = default;
private:
    void generate();
};

class Nis: public LinearOperatorSet
{
public:
    Nis(mBase& base_, std::string name_, ParamVector params_);
    ~Nis() = default;
private:
    void generate();
};

#endif // NI_H
