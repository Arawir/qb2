#include "g2.h"
#include "../bases/fockbase.h"


G2::G2(mBase &base_, std::string name_, ParamVector params_):
    LinearOperator{base_, name_, params_}
{
    generate();
}

void G2::generate()
{
    int m = params.getInt("m");
    int mp = params.getInt("mp");
    if(m==mp){
        base.add_nnm1(1.0,m,this->matrix);
//        matrix = base.nn(m,mp)-base.n(m);
    } else {
        base.add_nn(1.0,m,mp,this->matrix);
   //     matrix = base.nn(m,mp);
    }
}



G2s::G2s(mBase &base_, std::string name_, ParamVector params_):
    LinearOperatorSet{base_, name_, params_}
{
    generate();
}

void G2s::generate()
{
    int m = params.getInt("m");
    for(uint mp=0; mp<base.basicFockBase->M(); mp++){
        operators.push_back(G2{base,name+"_"+std::to_string(m), ParamVector{{std::to_string(m),std::to_string(mp)}, {"m","mp"}} });
    }
}
