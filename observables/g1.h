#ifndef G1_H
#define G1_H

#include "../operators/linearoperatorset.h"

class G1 : public LinearOperator
{
public:
    G1(mBase& base_, std::string name_, ParamVector params_);
    ~G1() = default;
private:
    void generate();
};

class G1s: public LinearOperatorSet
{
public:
    G1s(mBase& base_, std::string name_, ParamVector params_);
    ~G1s() = default;
private:
    void generate();
};
#endif // G1_H
