#include "momentum.h"
#include "../bases/fockbase.h"

using namespace std::complex_literals;

Momentum::Momentum(mBase &base_, std::string name_, ParamVector params_):
    cxLinearOperator{base_, name_, params_}
{
    generate();
}


void Momentum::generate()
{
    bool PBC = params.getDouble("PBC");

    for(int m=0; m<base.basicFockBase->M()-1; m++){
        base.add_b_bdag(-1i,m,m+1,this->matrix);
        base.add_bdag_b(+1i,m,m+1,this->matrix);
    }

    if(PBC){
        base.add_b_bdag(-1i,base.basicFockBase->M()-1,0,this->matrix);
        base.add_bdag_b(+1i,base.basicFockBase->M()-1,0,this->matrix);
    }
}
