#include "stringmanip.h"


bool contain(std::string text_, std::string toFind_)
{
    if (text_.find(toFind_) != std::string::npos){
        return true;
    }
    return false;
}

std::vector<std::string> split(std::string text_, std::string separator_)
{
    std::vector<std::string> out;
    size_t pos = 0;
    while ((pos = text_.find(separator_)) != std::string::npos) {
        out.push_back( text_.substr(0, pos) );
        text_.erase(0, pos + separator_.length());
    }
    out.push_back(text_);

    return out;
}
