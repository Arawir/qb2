#ifndef STORAGE_H
#define STORAGE_H

#include "../observables/ni.h"
#include "../observables/g1.h"
#include "../observables/g2.h"
#include "../observables/momentum.h"
#include "../hamiltonians/bhhamiltonian.h"
#include "../hamiltonians/ebhhamiltonian.h"
#include "../hamiltonians/rbhhamiltonian.h"
#include "userinterface.h"

class Storage{
public:
    std::list<mBase*> bases;
    std::vector<iLinearOperator*> linearOperators;
    std::list<StateSet*> states;
public:
    Storage(){ }
    ~Storage() = default;

    void clearWorkspace();
    bool baseExists(std::string name_);
    bool stateExists(std::string name_);
    bool operExists(std::string name_);
    mBase* findBase(std::string name_);
    iLinearOperator* findOperator(std::string name_);
    StateSet* findState(std::string name_);
};

#endif // STORAGE_H
