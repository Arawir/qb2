#ifndef USERINTERFACE_H
#define USERINTERFACE_H

#include <chrono>
#include <sys/time.h>
#include <ctime>
#include <exception>
#include <iostream>
#include <vector>
#include <armadillo>
#include "../bases/base.h"

std::ostream& operator<<(std::ostream& os, std::vector<double> dt);
bool commandIsReady();
std::tuple<std::string, std::vector<std::string>> getCommand();

void writeToOutput(std::string text,std::string msgType="DONE");
void writeToOutput(std::vector<double> data,std::string msgType="DONE");
void writeToOutput(std::vector<cpx> data,std::string msgType="DONE");
void writeToOutputM(arma::cx_mat matrix,std::string msgType="DONE");



struct MyException : public std::exception {
    std::string message;

    MyException(std::string message_);
    const char* what () const throw ();
};

void checkNumberOfParams(std::vector<std::string>& params, uint expectedNumber);
void checkNumberOfParams(std::vector<std::string>& params, uint expectedNumberMin, uint expectedNumberMax);
void clearOutput();

int process_mem_usage();
int process_peak_mem_usage();

#endif // USERINTERFACE_H
