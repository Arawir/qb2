#ifndef STRINGMANIP_H
#define STRINGMANIP_H

#include <iostream>
#include <vector>

bool contain(std::string text_, std::string toFind_);
std::vector<std::string> split(std::string text_, std::string separator_);
#endif // STRINGMANIP_H
