#include "userinterface.h"
#include "param.h"
#include <chrono>
#include <thread>


std::ostream& operator<<(std::ostream& os, std::vector<double> dt)
{
    for(int i=0; i<dt.size(); i++){
        os << dt[i] << " ";
    }
    return os;
}

bool commandIsReady(){
    std::ifstream fileCommandIn;
    std::string line;

    fileCommandIn.open("commands.txt");
    std::getline(fileCommandIn,line);
    std::size_t found = line.find(";");
    fileCommandIn.close();
    if (found!=std::string::npos) return true;
    return false;

}
std::tuple<std::string, std::vector<std::string>> getCommand(){
    std::ifstream fileCommandIn;
    std::string command;
    std::string tmp;
    std::vector<std::string> params;
    while(!commandIsReady()){
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }

    fileCommandIn.open("commands.txt");
    fileCommandIn >> command;
    fileCommandIn >> tmp;

    while(tmp != ";"){
        params.push_back(tmp);
        fileCommandIn >> tmp;
    }
    fileCommandIn.close();

    std::ofstream fileToClear("commands.txt");
    fileToClear.close();
    return {command, params};
}

void writeToOutput(std::string text,std::string msgType){
    std::ofstream fileOut("output.txt");

    fileOut << text << std::endl;
    fileOut << msgType<<" " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    fileOut.close();
}

void writeToOutput(std::vector<double> data,std::string msgType){
    std::ofstream fileOut("output.txt");

    for(auto& dat : data){
        fileOut << dat << " ";
    }
    fileOut << std::endl << msgType<<" " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    fileOut.close();
}

void writeToOutput(std::vector<cpx> data,std::string msgType){
    std::ofstream fileOut("output.txt");

    for(auto& dat : data){
        fileOut << dat << " ";
    }
    fileOut << std::endl << msgType<<" " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    fileOut.close();
}

void writeToOutputM(arma::cx_mat matrix,std::string msgType){
    std::ofstream fileOut("output.txt");

    fileOut << matrix;
    fileOut << std::endl << msgType<<" " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    fileOut.close();
}

MyException::MyException(std::string message_){ message=message_; }
const char* MyException::what () const throw () {
    writeToOutput(message,"ERROR");
    return message.c_str();
}

void checkNumberOfParams(std::vector<std::string>& params, uint expectedNumber){
    if(params.size() != expectedNumber){
        throw MyException("Wrong number of parameters: "+std::to_string(params.size())+" (expected "+std::to_string(expectedNumber)+") ");
    }
}



//ParamVector prepareParamVector(std::vector<std::string>& params, std::vector<std::string> names){
//    ParamVector out;
//    for(auto& param : params){
//        if( param.contins("=") ){

//        }
//    }
//}

void checkNumberOfParams(std::vector<std::string>& params, uint expectedNumberMin, uint expectedNumberMax){
    if(params.size() < expectedNumberMin || params.size() > expectedNumberMax){
        throw MyException("Wrong number of parameters: "+std::to_string(params.size())+" (expected "+std::to_string(expectedNumberMin)+"-"+std::to_string(expectedNumberMax)+") ");
    }
}

void clearOutput(){
    std::ofstream fileOut("output.txt");
    fileOut.close();
}


int process_mem_usage(){
    std::ifstream stat_stream("/proc/self/status",std::ios_base::in);
    int out;
    std::string tmp = "";
    while(tmp != "VmSize:"){
       stat_stream >> tmp;
    }
    stat_stream >> out;
    stat_stream.close();
    return out;
}

int process_peak_mem_usage(){
    std::ifstream stat_stream("/proc/self/status",std::ios_base::in);
    int out;
    std::string tmp = "";
    while(tmp != "VmPeak:"){
       stat_stream >> tmp;
    }
    stat_stream >> out;
    stat_stream.close();
    return out;
}
