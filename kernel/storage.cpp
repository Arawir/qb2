#include "storage.h"

void Storage::clearWorkspace(){
    for(auto& b : bases){
        delete b;
    }
    bases.clear();
    for(auto& s : states){
        delete s;
    }
    states.clear();
    for(auto& op : linearOperators){
        delete op;
    }
    linearOperators.clear();
}
bool Storage::baseExists(std::string name_){
    for(auto& base : bases){
        if(base->name==name_){
            return true;
        }
    }
    return false;
}
bool Storage::stateExists(std::string name_){
    for(StateSet* state : states){
        if(state->name==name_){
            return true;
        }
    }
    return false;
}
bool Storage::operExists(std::string name_){
    for(iLinearOperator* oper : linearOperators){
        if(oper->getname()==name_){
            return true;
        }
    }
    return false;
}
mBase* Storage::findBase(std::string name_){
    for(auto& base : bases){
        if(base->name==name_){
            return base;
        }
    }
    throw MyException("Wrong base name "+name_+" !");
    return nullptr;
}

iLinearOperator* Storage::findOperator(std::string name_){
    for(iLinearOperator* oper : linearOperators){
        if(oper->getname()==name_){
            return oper;
        }
    }
    throw MyException("Wrong operator name: "+name_+" !");
    return nullptr;
}

StateSet* Storage::findState(std::string name_){
    for(StateSet* state : states){
        if(state->name==name_){
            return state;
        }
    }
    throw MyException("Wrong state name: "+name_+" !");
    return nullptr;
}
