#include "param.h"
#include <cassert>

ParamVector::ParamVector(std::vector<std::string> commandParams_, std::vector<std::string> paramNames_)
{
    assert(commandParams_.size()==paramNames_.size() && "Wrong number of params!");

    bool eqSymbolUsed = false;

    for(int i=0; i<commandParams_.size(); i++){
        if( contain( commandParams_[i], "=") ){
            addParam( split(commandParams_[i],"=")[0],split(commandParams_[i],"=")[1] );
            eqSymbolUsed=true;
        } else {
            assert(!eqSymbolUsed && "Mixed params with = and without it!");
            addParam( paramNames_[i],commandParams_[i] );
        }
    }
}

void ParamVector::addParam(std::string name_, std::string value_)
{
    paramNames.push_back(name_);
    paramValues.push_back(value_);
}

void ParamVector::set(std::string name_, std::string value_)
{
    for(uint i=0; i<paramNames.size(); i++){
        if(paramNames[i]==name_){ paramValues[i]=value_; }
    }
    assert(false);
}

int ParamVector::getInt(std::string name_)
{
    for(uint i=0; i<paramNames.size(); i++){
        if(paramNames[i]==name_){ return atoi(paramValues[i].c_str()); }
    }
    assert(false);
    return 0;
}

double ParamVector::getDouble(std::string name_)
{
    for(uint i=0; i<paramNames.size(); i++){
        if(paramNames[i]==name_){ return atof(paramValues[i].c_str()); }
    }
    assert(false);
    return 0;
}

std::string ParamVector::getString(std::string name_)
{
    for(uint i=0; i<paramNames.size(); i++){
        if(paramNames[i]==name_){ return paramValues[i]; }
    }
    assert(false);
    return 0;
}

bool ParamVector::getBool(std::string name_)
{
    for(uint i=0; i<paramNames.size(); i++){
        if(paramNames[i]==name_){ return bool(atoi(paramValues[i].c_str())); }
    }
    assert(false);
    return 0;
}

std::string ParamVector::popString(std::string name_)
{
    std::string out;
    for(uint i=0; i<paramNames.size(); i++){

        if(paramNames[i]==name_){
            out = paramValues[i];
            paramValues.erase(paramValues.begin()+i);
            paramNames.erase(paramNames.begin()+i);
            return out;
        }
    }
    assert(false);
    return 0;
}
