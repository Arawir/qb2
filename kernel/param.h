#ifndef PARAM_H
#define PARAM_H

#include "stringmanip.h"

class ParamVector{
    std::vector<std::string> paramNames;
    std::vector<std::string> paramValues;
public:
    ParamVector(){ }
    ParamVector(std::vector<std::string> commandParams_, std::vector<std::string> paramNames_);
    ~ParamVector() = default;

    void addParam(std::string name_, std::string value_);
    void set(std::string name_, std::string value_);
    int getInt(std::string name_);
    double getDouble(std::string name_);
    std::string getString(std::string name_);
    bool getBool(std::string name_);

    std::string popString(std::string name_);
};

#endif // PARAM_H
