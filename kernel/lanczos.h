#ifndef LANCZOS_H
#define LANCZOS_H

#include <armadillo>
typedef unsigned long long int sluint;

void lanczos(sluint m, arma::vec& eigval, arma::mat& eigvec, const arma::sp_mat A);

#endif // LANCZOS_H
