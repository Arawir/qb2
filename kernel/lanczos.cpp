#include "lanczos.h"
#include <complex>

void lanczos(sluint m, arma::vec& eigval, arma::mat& eigvec, const arma::sp_mat A){
    std::vector<double> alpha(m), beta(m);

    arma::mat V{A.n_rows, m, arma::fill::none};
    V.col(0) = arma::normalise( arma::randu<arma::vec>(A.n_rows) );
    arma::vec wp = A*V.col(0);
    alpha[0] = arma::as_scalar(wp.t()*V.col(0));
    arma::vec w = wp - alpha[0]*V.col(0);

    for(sluint j=1; j<m; j++){
        beta[j] = arma::norm(w);
        if( abs(beta[j])>1E-16 ){ V.col(j) = w/beta[j]; }
        wp = A*V.col(j);
        alpha[j] = arma::as_scalar( wp.t()*V.col(j) );
        w = wp-alpha[j]*V.col(j)-beta[j]*V.col(j-1);
    }

    arma::mat T{m,m,arma::fill::zeros};
    for(sluint i=0;i<m-1;i++){
        T.at(i,i)=alpha[i];
        T.at(i,i+1)=beta[i+1];
        T.at(i+1,i)=beta[i+1];
    }
    T.at(m-1,m-1)=alpha[m-1];

    arma::mat eigvec2;
    arma::eig_sym(eigval, eigvec2, T);
    eigvec = V*eigvec2;
}


//liczy wariancje energii na wyznaczonych wektorach (na stanach własnych powinna być 0)
//std::vector<double> var(int neig, int dim, const gsl_matrix * VEC, const gsl_spmatrix * H){
//    std::vector<double> varlist(neig,0.0);
//    gsl_vector * v0 = gsl_vector_alloc (dim);
//    gsl_vector * v1 = gsl_vector_alloc (dim);
//    double xx, x;
//    for(int i=0;i<neig;i++){
//        gsl_matrix_get_col(v0,VEC,i);
//        gsl_spblas_dgemv(CblasNoTrans,0.5,H,v0,0.,v1);
//        gsl_spblas_dgemv(CblasTrans,0.5,H,v0,1.,v1);
//        xx=gsl_blas_dnrm2(v1);
//        gsl_blas_ddot(v1,v0,&x);
//        varlist[i]=xx*xx-x*x;
//    }
//    gsl_vector_free(v0);
//    gsl_vector_free(v1);
//    return varlist;
//}
