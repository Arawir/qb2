#ifndef BASE_H
#define BASE_H

#include "mbase.h"

class EigensBase : public mBase
{
public:
    EigensBase(std::string name_);
    ~EigensBase() = default;
    sluint size(){ assert(false); }
    sluint particlesInOrbital(luint state_, luint m_){ assert(false); }
    sluint indexOfState(luint state_){ assert(false); }

    void add_b_bdag(double val_, luint m_, luint mp_, arma::sp_mat& matrix_){ assert(false); }
    void add_bdag_b(double val_, luint m_, luint mp_, arma::sp_mat& matrix_){ assert(false); }
    void add_n(double val_, luint m_, arma::sp_mat& matrix_){ assert(false); }
    void add_nn(double val_, luint m_, luint mp_, arma::sp_mat& matrix_){ assert(false); }
    void add_nnm1(double val_, luint m_, arma::sp_mat& matrix_){ assert(false); }

    void add_b_bdag(cpx val_, luint m_, luint mp_, arma::sp_cx_mat& matrix_){ assert(false); }
    void add_bdag_b(cpx val_, luint m_, luint mp_, arma::sp_cx_mat& matrix_){ assert(false); }
    void add_n(cpx val_, luint m_, arma::sp_cx_mat& matrix_){ assert(false); }
    void add_nn(cpx val_, luint m_, luint mp_, arma::sp_cx_mat& matrix_){ assert(false); }
    void add_nnm1(cpx val_, luint m_, arma::sp_cx_mat& matrix_){ assert(false); }

    void printStates(){ assert(false); }

    bool operator==(FockBase& baseR_){ assert(false);}
    void save(std::string saveName_){ assert(false);}
    void load(std::string loadName_){ assert(false);}
};



#endif // BASE_H
