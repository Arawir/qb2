#ifndef FOCKBASE_H
#define FOCKBASE_H

#include "mbase.h"
#include "../kernel/param.h"

class FockBase: public mBase //now conserves number of partilces
{
public:
    ParamVector params;
    std::vector<luint> usedStates;
public:
    FockBase(std::string name_, ParamVector params_);
    ~FockBase() = default;

    sluint particlesInOrbital(luint state_, luint m_);
    sluint indexOfState(luint state_);

    void add_b_bdag(double val_, luint m_, luint mp_, arma::sp_mat& matrix_);
    void add_bdag_b(double val_, luint m_, luint mp_, arma::sp_mat& matrix_);
    void add_n(double val_, luint m_, arma::sp_mat& matrix_);
    void add_nn(double val_, luint m_, luint mp_, arma::sp_mat& matrix_);
    void add_nnm1(double val_, luint m_, arma::sp_mat& matrix_);

    void add_b_bdag(cpx val_, luint m_, luint mp_, arma::sp_cx_mat& matrix_);
    void add_bdag_b(cpx val_, luint m_, luint mp_, arma::sp_cx_mat& matrix_);
    void add_n(cpx val_, luint m_, arma::sp_cx_mat& matrix_);
    void add_nn(cpx val_, luint m_, luint mp_, arma::sp_cx_mat& matrix_);
    void add_nnm1(cpx val_, luint m_, arma::sp_cx_mat& matrix_);

    void generate();
    sluint size();

    void printStates();

    uint M();
    uint N();
    int maxOcc();

    bool operator==(FockBase& baseR_);
    void save(std::string saveName_);
    void load(std::string loadName_);
private:
    void generateSubstate(int Nall, int M, int N, luint prefix);
    void generateSubstate(int Nall, int M, int N, int maxOcc, luint prefix);
};

#endif // FOCKBASE_H
