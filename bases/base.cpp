#include "base.h"
#include <cassert>

EigensBase::EigensBase(std::string name_) :
    mBase{name_}
{
    name=name_;
}

TransferMatrix::TransferMatrix(bool _isUnit):
    isUnit{_isUnit}
  , BToFockMatrix{}
  , FockToBMatrix{}
{

}

TransferMatrix::TransferMatrix(arma::mat &_BtoFock):
    isUnit{false}
  , BToFockMatrix{_BtoFock}
  , FockToBMatrix{arma::inv(_BtoFock)}
{

}
