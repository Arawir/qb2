#include "mbase.h"

mBase::mBase(const mBase &base_,std::string name_)
{
    name = name_;
    transferMatrix = base_.transferMatrix;
    basicFockBase = base_.basicFockBase;
    eigenValues = base_.eigenValues;
}
