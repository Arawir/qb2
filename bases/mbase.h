#ifndef MBASE_H
#define MBASE_H

#include <armadillo>
#include <vector>
#include <cassert>
#include <boost/multiprecision/cpp_int.hpp>

typedef std::complex<double> cpx;
//typedef unsigned long long int luint;
typedef unsigned long long int sluint;
typedef boost::multiprecision::uint256_t luint;

class mBase;
class FockBase;

struct TransferMatrix{
    bool isUnit = false;
    arma::mat BToFockMatrix;
    arma::mat FockToBMatrix;
public:
    TransferMatrix(){ }
    TransferMatrix(bool _isUnit);
    TransferMatrix(arma::mat& _BtoFock);
};

class mBase{
public:
    std::string name;
    TransferMatrix transferMatrix;
    FockBase* basicFockBase;
    arma::vec eigenValues; //only for eigenbase
public:
    mBase(std::string _name){ }
    mBase(const mBase& base_, std::string name_);
    virtual ~mBase() = default;

    virtual sluint size() = 0;
    virtual sluint particlesInOrbital(luint state_, luint m_) = 0;
    virtual sluint indexOfState(luint state_) = 0;

    virtual void add_b_bdag(double val_, luint m_, luint mp_, arma::sp_mat& matrix_) = 0;
    virtual void add_bdag_b(double val_, luint m_, luint mp_, arma::sp_mat& matrix_) = 0;
    virtual void add_n(double val_, luint m_, arma::sp_mat& matrix_) = 0;
    virtual void add_nn(double val_, luint m_, luint mp_, arma::sp_mat& matrix_) = 0;
    virtual void add_nnm1(double val_, luint m_, arma::sp_mat& matrix_) = 0;

    virtual void add_b_bdag(cpx val_, luint m_, luint mp_, arma::sp_cx_mat& matrix_) = 0;
    virtual void add_bdag_b(cpx val_, luint m_, luint mp_, arma::sp_cx_mat& matrix_) = 0;
    virtual void add_n(cpx val_, luint m_, arma::sp_cx_mat& matrix_) = 0;
    virtual void add_nn(cpx val_, luint m_, luint mp_, arma::sp_cx_mat& matrix_) = 0;
    virtual void add_nnm1(cpx val_, luint m_, arma::sp_cx_mat& matrix_) = 0;

    virtual void printStates() = 0;

    virtual void save(std::string saveName_)=0;
    virtual void load(std::string loadName_)=0;
};

#endif // MBASE_H
