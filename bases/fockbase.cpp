#include "fockbase.h"

luint mpow(luint x_, luint y_){
    luint out = 1;
    for(; y_ >0; y_--){
        out *= x_;
    }
    return out;
}

FockBase::FockBase(std::string name_, ParamVector params_) :
    mBase{name_}
  , params{params_}
{
    generate();
    name = name_;
    transferMatrix.isUnit = true;
    basicFockBase = this;
}

sluint FockBase::particlesInOrbital(luint state_, luint m_)
{
    uint M = params.getInt("M");
    uint N = params.getInt("N");
    return sluint( (state_ % mpow(N+1,(M-m_))) / mpow(N+1,(M-m_-1)) );
}

sluint FockBase::indexOfState(luint state_)
{
    uint l=0;
    uint r=usedStates.size()-1;

    if(usedStates[r]==state_){ return r; }
    if(usedStates[l]==state_){ return l; }

    while(r-l>1){
        uint pivot = (r+l)/2;
        if(usedStates[pivot]==state_){
            return pivot;
        } else if(usedStates[pivot]<state_){
            l=pivot;
        } else {
            r=pivot;
        }
    }
    std::cout << usedStates.back() << std::endl;
    std::cout << state_ << std::endl;
    assert(false);
    return 0;
}

void FockBase::add_b_bdag(double val_, luint m_, luint mp_, arma::sp_mat &matrix_)
{
    uint M = params.getInt("M");
    uint N = params.getInt("N");
    for(sluint stateInIndex=0; stateInIndex<usedStates.size(); stateInIndex++){
        luint state = usedStates[stateInIndex];
        if(particlesInOrbital(state,m_)>0){
            if( (particlesInOrbital(state,mp_)<maxOcc()) || (maxOcc()==-1)){
                luint stateOut = state-1*mpow(N+1,(M-m_-1))+1*mpow(N+1,(M-mp_-1));
                if(m_==mp_){
                    matrix_(stateInIndex,indexOfState(stateOut)) += val_*sqrt( 1+particlesInOrbital(state,m_)* (1+particlesInOrbital(state,mp_)));
                } else {
                    matrix_(stateInIndex,indexOfState(stateOut)) += val_*sqrt( particlesInOrbital(state,m_)* (1+particlesInOrbital(state,mp_)));
                }
            }
        }
    }
}

void FockBase::add_bdag_b(double val_, luint m_, luint mp_, arma::sp_mat &matrix_)
{
    uint M = params.getInt("M");
    uint N = params.getInt("N");
    for(sluint stateInIndex=0; stateInIndex<usedStates.size(); stateInIndex++){
        luint state = usedStates[stateInIndex];
        if(particlesInOrbital(state,mp_)>0){
            if( (particlesInOrbital(state,m_)<maxOcc()) || (maxOcc()==-1)){
                luint stateOut = state+1*mpow(N+1,(M-m_-1))-1*mpow(N+1,(M-mp_-1));
                if(m_==mp_){
                    matrix_(stateInIndex,indexOfState(stateOut)) += val_*sqrt( (particlesInOrbital(state,m_))* particlesInOrbital(state,mp_) );
                } else {
                    matrix_(stateInIndex,indexOfState(stateOut)) += val_*sqrt( (1+particlesInOrbital(state,m_))* particlesInOrbital(state,mp_) );
                }
            }
        }
    }
}

void FockBase::add_n(double val_, luint m_, arma::sp_mat &matrix_)
{
    for(sluint stateInIndex=0; stateInIndex<usedStates.size(); stateInIndex++){
        luint state = usedStates[stateInIndex];
        matrix_(stateInIndex,stateInIndex) += val_*particlesInOrbital(state,m_);
    }
}

void FockBase::add_nn(double val_, luint m_, luint mp_, arma::sp_mat &matrix_)
{
    for(sluint stateInIndex=0; stateInIndex<usedStates.size(); stateInIndex++){
        luint state = usedStates[stateInIndex];
        if( (particlesInOrbital(state,m_)>0) && (particlesInOrbital(state,mp_)>0)){
            matrix_(stateInIndex,stateInIndex) += val_*particlesInOrbital(state,m_)*particlesInOrbital(state,mp_) ;
        }
    }
}

void FockBase::add_nnm1(double val_, luint m_, arma::sp_mat &matrix_)
{
    for(sluint stateInIndex=0; stateInIndex<usedStates.size(); stateInIndex++){
        luint state = usedStates[stateInIndex];
        matrix_(stateInIndex,stateInIndex) += val_*particlesInOrbital(state,m_)*(particlesInOrbital(state,m_)-1);
    }
}

/////////////////////////////////////////

void FockBase::add_b_bdag(cpx val_, luint m_, luint mp_, arma::sp_cx_mat &matrix_)
{
    uint M = params.getInt("M");
    uint N = params.getInt("N");
    for(sluint stateInIndex=0; stateInIndex<usedStates.size(); stateInIndex++){
        luint state = usedStates[stateInIndex];
        if(particlesInOrbital(state,m_)>0){
            if( (particlesInOrbital(state,mp_)<maxOcc()) || (maxOcc()==-1)){
                luint stateOut = state-1*mpow(N+1,(M-m_-1))+1*mpow(N+1,(M-mp_-1));
                if(m_==mp_){
                    matrix_(stateInIndex,indexOfState(stateOut)) += val_*sqrt( 1+particlesInOrbital(state,m_)* (1+particlesInOrbital(state,mp_)));
                } else {
                    matrix_(stateInIndex,indexOfState(stateOut)) += val_*sqrt( particlesInOrbital(state,m_)* (1+particlesInOrbital(state,mp_)));
                }
            }
        }
    }
}

void FockBase::add_bdag_b(cpx val_, luint m_, luint mp_, arma::sp_cx_mat &matrix_)
{
    uint M = params.getInt("M");
    uint N = params.getInt("N");
    for(sluint stateInIndex=0; stateInIndex<usedStates.size(); stateInIndex++){
        luint state = usedStates[stateInIndex];
        if(particlesInOrbital(state,mp_)>0){
            if( (particlesInOrbital(state,m_)<maxOcc()) || (maxOcc()==-1)){
                luint stateOut = state+1*mpow(N+1,(M-m_-1))-1*mpow(N+1,(M-mp_-1));
                if(m_==mp_){
                    matrix_(stateInIndex,indexOfState(stateOut)) += val_*sqrt( (particlesInOrbital(state,m_))* particlesInOrbital(state,mp_) );
                } else {
                    matrix_(stateInIndex,indexOfState(stateOut)) += val_*sqrt( (1+particlesInOrbital(state,m_))* particlesInOrbital(state,mp_) );
                }
            }
        }
    }
}

void FockBase::add_n(cpx val_, luint m_, arma::sp_cx_mat &matrix_)
{
    for(sluint stateInIndex=0; stateInIndex<usedStates.size(); stateInIndex++){
        luint state = usedStates[stateInIndex];
        matrix_(stateInIndex,stateInIndex) += val_*double(particlesInOrbital(state,m_));
    }
}

void FockBase::add_nn(cpx val_, luint m_, luint mp_, arma::sp_cx_mat &matrix_)
{
    for(sluint stateInIndex=0; stateInIndex<usedStates.size(); stateInIndex++){
        luint state = usedStates[stateInIndex];
        if( (particlesInOrbital(state,m_)>0) && (particlesInOrbital(state,mp_)>0)){
            matrix_(stateInIndex,stateInIndex) += val_*double(particlesInOrbital(state,m_)*particlesInOrbital(state,mp_)) ;
        }
    }
}

void FockBase::add_nnm1(cpx val_, luint m_, arma::sp_cx_mat &matrix_)
{
    for(sluint stateInIndex=0; stateInIndex<usedStates.size(); stateInIndex++){
        luint state = usedStates[stateInIndex];
        matrix_(stateInIndex,stateInIndex) += val_*double(particlesInOrbital(state,m_)*(particlesInOrbital(state,m_)-1));
    }
}

///////////////////////////////////////////////
void FockBase::generateSubstate(int Nall, int M, int N, luint prefix)
{
    if(M==1){
        usedStates.push_back(prefix*(Nall+1)+N);
    } else {
        for(int N0=0; N0<=N; N0++){
            generateSubstate(Nall,M-1,N-N0,prefix*(Nall+1)+N0);
        }
    }
}

void FockBase::generateSubstate(int Nall, int M, int N, int maxOcc, luint prefix)
{
    if( (M==1)){
        if(N<=maxOcc){
            usedStates.push_back(prefix*(Nall+1)+N);
        }
    } else {
        for(int N0=0; N0<=std::min(N,maxOcc); N0++){
            generateSubstate(Nall,M-1,N-N0,maxOcc,prefix*(Nall+1)+N0);
        }
    }
}

void FockBase::generate()
{
    uint M = params.getInt("M");
    uint N = params.getInt("N");
    if( (maxOcc()==-1) || (maxOcc()>=N) ){
        generateSubstate(N,M,N,0);
    } else {
        generateSubstate(N,M,N,maxOcc(),0);
    }
}

sluint FockBase::size()
{
    return sluint(usedStates.size());
}

void FockBase::printStates()
{
    for(auto &state : usedStates){
        std::cout << "|";
        for(uint m=0; m<M();m++){
            std::cout << particlesInOrbital(state,m);
        }
        std::cout << ">" << std::endl;
    }
}

uint FockBase::M()
{
    return params.getInt("M");
}

uint FockBase::N()
{
    return params.getInt("N");
}

int FockBase::maxOcc()
{
    return params.getInt("maxOcc");
}

bool FockBase::operator==(FockBase &baseR_)
{
    uint M = params.getInt("M");
    uint N = params.getInt("N");
    if(N != baseR_.params.getInt("N")) return false;
    if(M != baseR_.params.getInt("M")) return false;
    return true;
}

void FockBase::save(std::string saveName_)
{
    if(saveName_==""){ saveName_ = name; }
    saveName_ += ".base";

}

void FockBase::load(std::string loadName_)
{

}
